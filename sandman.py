#!/usr/bin/env python

import os
import sys

import rospy
import rosbag
import cv2
import cv_bridge
import numpy as np
import matplotlib.pyplot as plt
import rospy
import tf.transformations as tft
from position_control import *

from kinova_msgs.msg import PoseVelocity
import kinova_msgs.msg
import kinova_msgs.srv
import std_msgs.msg
import std_srvs.srv
import geometry_msgs.msg
import sensor_msgs.msg
from std_msgs.msg import String
from std_msgs.msg import Int32MultiArray

min_blue = np.array([110,0,0])
max_blue = np.array([255,255,56])

kernel = np.ones((2,2), np.uint8)
num_iter = 2
point_reached = False

CURR_POSE = None
centroid = None
vel = None
depth_cam_to_tool = 0.4
im_size = [480, 640]
points = None

def send_robot_to_home():
    #goal_joint_pos = [371.0, 220.0, 70.0, 179.0, 58.0, 38.0, 0.0]
    goal_joint_pos = [371.0, 215.0, 50.0, 179.0, 58.0, 43.0, 0.0]
    move_to_joint_position(goal_joint_pos)
    print "1st home reached"
    import time
    time.sleep(0.5)
    #goal_joint_pos = [371.0, 233.0, 70.0, 179.0, 58.0, 38.0, 0.0]
    goal_joint_pos = [371.0, 224, 50.0, 179.0, 58.0, 43.0, 0.0]
    move_to_joint_position(goal_joint_pos)
    print "2nd home reached"
    
def find_blue(msg):
    global vel, im_size, point_reached
    global centroid, state

    cvb = cv_bridge.CvBridge()
    # Convert into opencv matrix
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')
    im_size = img.shape

    # Threshold the image for blue
    binary_blue = cv2.inRange(img, min_blue, max_blue)
    eroded_blue = cv2.erode(binary_blue, kernel, iterations=num_iter)
    dilated_blue = cv2.dilate(eroded_blue, kernel, iterations=num_iter)

    # Find the contours - saved in blue_contours and red_contours
    im2_b, blue_contours, hierachy_blue = cv2.findContours(dilated_blue.copy(), cv2.RETR_TREE,
                                                         cv2.CHAIN_APPROX_SIMPLE)
 
    if len(blue_contours) > 0 and blue_contours is not None:
        centroid = detecting_centroids(blue_contours)
        if centroid is not None:
            cv2.circle(img, (centroid[0], centroid[1]), 5, (255,0,0),-1)
    else:
        centroid = None



    enable_force = True
    
    if state == 0: #Waiting for command
        pass
    elif state == 1: #VS to A
        if not point_reached:
            print("VS to A")
            goal_x = points[0]
            goal_y = points[1]
            goal_point = [goal_x, goal_y]
            cv2.circle(img, (goal_point[0], goal_point[1]), 5, (0,0,255),-1)
            servo_to_point(goal_point, depth_cam_to_tool, enable_force)
        else:
            point_reached = False
            state = 2
    elif state == 2: #VS to B
        if not point_reached:
            print("VS to B")
            goal_x = points[2]
            goal_y = points[3]
            goal_point = [goal_x, goal_y]
            cv2.circle(img, (goal_point[0], goal_point[1]), 5, (0,0,255),-1)
            servo_to_point(goal_point, depth_cam_to_tool, enable_force)
        else:
            point_reached = False
            state = 3
    elif state == 3: #Home
        point_reached = False
        #goto_relative_height(0.1)
        send_robot_to_home()
        state = 0

    if points != None:
        cv2.circle(img, (points[0], points[1]), 5, (70,70,255), -1)
        cv2.circle(img, (points[2], points[3]), 5, (255,70,70), -1)
    cv2.imshow('Blue', img)
    cv2.waitKey(1)


def servo_to_point(desired_centroid, depth, enable_z):
    global vel, point_reached, CURR_FORCE
    if centroid:
        cx_b = centroid[0]; cy_b = centroid[1];
        current_centroid = np.array([cx_b, cy_b])

        vel_z = 0.0
        
        thresh = 10
        max_norm = 0.05
        e = current_centroid - desired_centroid
        error_norm =np.linalg.norm(e) 
        if error_norm > thresh:            
            interaction = get_interaction(current_centroid, im_size, depth)
            i_inv = np.linalg.pinv(interaction)

            gain = 0.008
            velocity_cam = np.matmul(i_inv, e)
            # velocity_b = cam_to_base(velocity_cam)[:2]
            velocity_b = velocity_cam
            velocity_b[1] = -velocity_b[1]

            velocity_b = gain*velocity_b
            norm = np.linalg.norm(velocity_b)
            if norm > max_norm:
                velocity_b = (velocity_b / norm) * max_norm

            if enable_z:
                # Contact Management
                desired_force = -13.0
                force_error = desired_force - CURR_FORCE
                K_z = 0.008
                vel_z = K_z * force_error        
                max_vel_z = 0.03                    
                vel_z = np.clip(vel_z, -max_vel_z, max_vel_z)                    
        else:
            point_reached = True
            #print("Point Reached?", point_reached)
            velocity_b = np.array([0, 0])
    else:
        print("No Assigned Centroid")
        velocity_b = np.array([0, 0])

    #print(vel)
    print vel_z
    vel = PoseVelocity()
    vel.twist_linear_x = velocity_b[0]
    vel.twist_linear_y = velocity_b[1]
    vel.twist_linear_z = vel_z

def goto_relative_height(height_delta):
    global point_reached
    goal = CURR_POSE
    pos = [goal.position.x, goal.position.y, goal.position.z + height_delta]
    ori = [goal.orientation.x, goal.orientation.y, goal.orientation.z, goal.orientation.w]
    #ori = [-0.13, -0.69, 0.277, 0.655]

    move_to_position(pos, ori)
    
    point_reached = False

def cam_to_base(velocity):
    # Camera position in base frame
    t= np.array([[-0.51],
                 [-0.05],
                 [0.45 ]])

    transform = np.array([[1, 0,  0,  0,     t[2], -t[1]],
                          [0, -1, 0,  t[2],  0,     t[0]],
                          [0, 0,  -1, -t[1], -t[0], 0   ],
                          [0, 0,  0,  1,     0,     0   ],
                          [0, 0,  0,  0,     -1,    0   ],
                          [0, 0,  0,  0,     0,     -1  ]])

    velocity_b = np.matmul(transform, velocity)

    return velocity_b
    
def get_interaction(centroid, im_size, depth):
    im_width  = im_size[1]
    im_height = im_size[0]
    # Camera to box depth for vs
    Z = depth
    pixel_to_meter = 0.0000036 # Size of pixel in meters
    focal_in_pixels = 430

    # Convert to world frame
    x = (centroid[0] - im_width/2)/focal_in_pixels
    y = (centroid[1] - im_height/2)/focal_in_pixels

    # L = np.array([[-1/Z, 0,    x/Z, x*y,      -(1 + x**2), y],
    #               [0,    -1/Z, y/Z, 1 + y**2, -x*y,       -x]])
 
    L = np.array([[-1/Z, 0],#,    x/Z, x*y,      -(1 + x**2), y],
                  [0,    -1/Z]])#, y/Z, 1 + y**2, -x*y,       -x]])
   
  
    return L
    
def detecting_centroids(contours):
    list_boxes=[]
    minimum_area=100
    cx=[]
    cy=[]
    # Find the bounding box for each contour
    for contour in contours:
        x,y,w,h = cv2.boundingRect(contour)
        cx_ = x+(w/2)
        cy_ = y+(h/2)
        area = w*h
        if area > minimum_area:
            list_boxes.append((cx_, cy_, area))

    # Draw centroid of largest blob
    if len(list_boxes) > 0:
        # Sort based on area
        list_boxes.sort(key=lambda x: x[2], reverse=True)
        cx = list_boxes[0][0]
        cy = list_boxes[0][1]
        centroid = (cx, cy)
    else:
        centroid = None

    return centroid


def robot_position_callback(msg):
    # Monitor robot position.
    global CURR_POSE
    CURR_POSE = msg.pose

def force_callback(msg):
    global CURR_FORCE
    CURR_FORCE = msg.wrench.force.z

def points_callback(data):
    global points
    if state == 0:
        points = data.data
        # xs = [points[0], points[2]]
        xs = np.clip([points[0], points[2]], 300, 640)
        ys = np.clip([points[1], points[3]], 60, 420)
        points = [xs[0],ys[0], xs[1],ys[1]]
        # print("NETWORK OUT", points)

def command_generator_callback(msg):
    print "Rcvd Command: " + msg.data

    global state
    global point_reached
    
    if msg.data == "a":
        if state == 0:
            state = 1
            point_reached = False
            print "Enabling Robot Action"
    elif msg.data == "h":
        state = 3
        print "Going Home"
        
if __name__ == '__main__':
    rospy.init_node('analisi_img',anonymous=True) # node name

    global state
    state = 0
    
    # 1) Definisco publisher and subscriber
    image_sub = rospy.Subscriber('/camera/rgb/image_raw', sensor_msgs.msg.Image, find_blue, queue_size=1) #listen robot position
    point_sub = rospy.Subscriber('/points', Int32MultiArray, points_callback, queue_size=1) #listen robot position

    # pub=rospy.Publisher('camera',String,queue_size=1)# Publish of the image information

    vel_pub = rospy.Publisher('/m1n6s200_driver/in/cartesian_velocity', PoseVelocity, queue_size=10)
    position_sub = rospy.Subscriber('/m1n6s200_driver/out/tool_pose', geometry_msgs.msg.PoseStamped, robot_position_callback, queue_size=1)
    force_sub = rospy.Subscriber('/m1n6s200_driver/out/tool_wrench', geometry_msgs.msg.WrenchStamped, force_callback)

    command_sub = rospy.Subscriber('/commands', std_msgs.msg.String, command_generator_callback)

    #rospy.spin()
    send_robot_to_home()

    r = rospy.Rate(100)
    while not rospy.is_shutdown():
        if vel is not None:
            # print(vel)
            if not point_reached:
                pass
                vel_pub.publish(vel)
        r.sleep()
