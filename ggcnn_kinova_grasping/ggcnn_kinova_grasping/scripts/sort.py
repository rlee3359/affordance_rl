#! /usr/bin/env python

from __future__ import print_function
import rospy
import tf.transformations as tft
from gym import spaces

import numpy as np
import cv_bridge
import cv2
from kinova_msgs.msg import PoseVelocity
from kinova_msgs.msg import JointAngles

import kinova_msgs.msg
import kinova_msgs.srv
import sensor_msgs.msg
from sensor_msgs.msg import Image
import std_msgs.msg
import std_srvs.srv
import geometry_msgs.msg
import darknet_ros_msgs.msg

from helpers.gripper_action_client import set_finger_positions
from helpers.position_action_client import position_client, move_to_position, move_to_joint_position, kill_actions
from helpers.transforms import current_robot_pose, publish_tf_quaterion_as_transform, convert_pose, publish_pose_as_transform
from helpers.covariance import generate_cartesian_covariance

import new_agent as agent
# import no_deep_sets as agent
import copy
import time
import torch

np.random.seed(0)


def calibrate_depth():
    cvb = cv_bridge.CvBridge()
    cv2.namedWindow('image')
    def callback(x): pass

    # create trackbars for color change
    cv2.createTrackbar('Depth','image',0,1000,callback)

    while(1):
        msg = rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
        img = cvb.imgmsg_to_cv2(msg)

        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        # get current positions of four trackbars
        # d = cv2.getTrackbarPos('Depth','image')
        # d = float(d)/1000
        d = 0.265
        grasp_cut = 0.03

        print("Depth", d)

        _, mask = cv2.threshold(img, d,1, cv2.THRESH_BINARY_INV)
        # print("MeanMask", np.mean(mask))
        # print(np.mean(mask) > 0.03)

        print(np.mean(mask))
        cv2.imshow('image', mask)


def get_object_detections():
    boxes = []

    msg = rospy.wait_for_message('/darknet_ros/bounding_boxes', darknet_ros_msgs.msg.BoundingBoxes)
    boxes.extend(msg.bounding_boxes)
    one = []; two = []; three = []

    def to_xywh(box):
        x = box.xmin
        y = box.ymin
        w = box.xmax - box.xmin
        h = box.ymax - box.ymin
        return [x,y,w,h]

    for box in boxes:
        if box.Class == "bottle":# or box.Class == "toothbrush":
            one.append(to_xywh(box))
        elif box.Class == "cup" or box.Class == "bowl" or box.Class == "fork" or box.Class == "spoon" or box.Class == "toothbrush":
            two.append(to_xywh(box))
        if box.Class == "teddy bear" or box.Class == "fork":
            three.append(to_xywh(box))

    return one, two, three



def detect_grasp():
    # return True
    print("Detecting Grasp")
    cvb = cv_bridge.CvBridge()
    msg = rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
    img = cvb.imgmsg_to_cv2(msg)

    d = 0.265
    grasp_cut = 0.04

    _, mask = cv2.threshold(img, d,1, cv2.THRESH_BINARY_INV)
    print(np.mean(mask))
    return np.mean(mask) > 0.06

def calibrate_colors():
    cvb = cv_bridge.CvBridge()
    cv2.namedWindow('image')
    def callback(x): pass

    # create trackbars for color change
    cv2.createTrackbar('R High','image',0,255,callback)
    cv2.createTrackbar('R Low','image',0,255,callback)
    cv2.createTrackbar('G High','image',0,255,callback)
    cv2.createTrackbar('G Low','image',0,255,callback)
    cv2.createTrackbar('B High','image',0,255,callback)
    cv2.createTrackbar('B Low','image',0,255,callback)

    while(1):
        msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
        img = cvb.imgmsg_to_cv2(msg, 'bgr8')


        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        # get current positions of four trackbars
        r_low = cv2.getTrackbarPos('R Low','image')
        r_hi = cv2.getTrackbarPos('R High','image')
        g_low = cv2.getTrackbarPos('G Low','image')
        g_hi = cv2.getTrackbarPos('G High','image')
        b_low = cv2.getTrackbarPos('B Low','image')
        b_hi = cv2.getTrackbarPos('B High','image')
        min_colors = np.array([b_low, g_low, r_low])
        max_colors = np.array([b_hi, g_hi, r_hi])

        print("Min colors", min_colors)
        print("Max colors", max_colors)

        binary  = cv2.inRange(img, min_colors, max_colors)
        eroded  = cv2.erode(binary, kernel, iterations=num_iter)
        dilated = cv2.dilate(eroded, kernel, iterations=num_iter)

        viz_img = img.copy()
        viz_img[dilated <= 0] = 0
        cv2.imshow('image', viz_img)


MOVING = False  # Flag whether the robot is moving under velocity control.
CURR_Z = 0  # Current end-effector z height.
CURR_POS = None
CURR_ANG = None

kernel = np.ones((2,2), np.uint8)
num_iter = 2
im_size = [480, 640]

def find_color(img, min_colors, max_colors, min_area):
    global vel, im_size, point_reached
    global centroid, state

    # Threshold the image for blue
    binary  = cv2.inRange(img, min_colors, max_colors)
    eroded  = cv2.erode(binary, kernel, iterations=num_iter)
    dilated = cv2.dilate(eroded, kernel, iterations=num_iter)
    # cv2.imshow("Thresh", dilated)

    # Find the contours - saved in blue_contours and red_contours
    im2, contours, hierachy= cv2.findContours(dilated.copy(), cv2.RETR_TREE,
                                                         cv2.CHAIN_APPROX_SIMPLE)

    boxes = []
    if len(contours) > 0 and contours is not None:
        boxes = detect_centroids(contours, min_area)

    return boxes

def detect_centroids(contours, minimum_area):
    boxes = []
    # minimum_area=8000
    cx=[]
    cy=[]
    # Find the bounding box for each contour
    for contour in contours:
        x,y,w,h = cv2.boundingRect(contour)
        area = w*h
        if area > minimum_area:
            boxes.append([x,y,w,h])

    return boxes


def get_image():
    cvb = cv_bridge.CvBridge()
    msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')
    img = cv2.resize(img, (84,84))
    cv2.imshow('img', img)

    msg = rospy.wait_for_message('/ggcnn/img/grasp_img', sensor_msgs.msg.Image)
    grasps = cvb.imgmsg_to_cv2(msg)
    grasps = cv2.resize(grasps, (84, 84))
    cv2.imshow('grasp', grasps)
    cv2.waitKey(10)

def vision(polygon, yolo=True):
    # print("Vision")
    import time
    rospy.sleep(0.1)
    msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
    grasps = rospy.wait_for_message('/ggcnn/img/grasps', std_msgs.msg.String)
    grasps = grasps.data.split('|')
    # TODO:
    # Jake:
    # not sure if this is the right thing to do, but it'll keep last night's crash from happening
    try:
        grasps = [[float(g) for g in grasp.split(',')] for grasp in grasps]
    except ValueError:
        return [],[],[]
    # TODO:

    min_blue = np.array([42,0,0])
    max_blue = np.array([255,95,31])

    min_red = np.array([0,0,31])
    max_red = np.array([33,76,255])

    min_green = np.array([0,0,2])
    max_green = np.array([50,255,53])

    cvb = cv_bridge.CvBridge()
    # Convert into opencv matrix
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')


    if yolo==False:
        # Find colors
        red_boxes = find_color(img, min_red, max_red, 5000)
        green_boxes = find_color(img, min_green, max_green, 4000)
        blue_boxes = find_color(img, min_blue, max_blue, 6000)
    else:
        # Find colors (AKA not colors)
        red_boxes, green_boxes, blue_boxes = get_object_detections()



    im_size = img.shape
    mask = np.ones(im_size)
    polygon = sorted(polygon, key = lambda x: x[0])
    cv2.polylines(img, [np.int32(np.array(polygon))], True, (0,255,255))
    cv2.fillConvexPoly(mask, np.asarray(polygon), (0,0,0))
    img = cv2.addWeighted(img/255.0, 0.8, 1-mask, 0.3, -0.3)
    # img[mask[:]!=(255,255,255)] = 0

    # Display boxes
    for box in blue_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 1)

    for box in red_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,0,255), 1)

    for box in green_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,255,0), 1)


    def box_in_poly(box):
        pts = [[box[0], box[1]], [box[0], box[1]+box[3]], [box[0]+box[2], box[1]+box[3]], [box[0]+box[2], box[1]]]
        p_mask = np.zeros(im_size)
        rect_mask = np.zeros(im_size)
        cv2.fillConvexPoly(p_mask, np.asarray(polygon), (255,255,255))
        cv2.fillConvexPoly(rect_mask, np.asarray(pts), (255,255,255))
        intersect = cv2.bitwise_and(p_mask, rect_mask)
        return np.sum(intersect) > np.sum(rect_mask)/3

        # pts = [(box[0], box[1]), (box[0], box[1]+box[3]), (box[0]+box[2], box[1]), (box[0]+box[2], box[1]+box[3])]
        # num_in = [cv2.pointPolygonTest(np.array([polygon]), pt, False) for pt in pts]
        # num_in = sum([i for i in num_in if i >= 0])
        # return num_in > 1

    blue_boxes  = [box for box in blue_boxes  if box_in_poly(box)]
    red_boxes   = [box for box in red_boxes   if box_in_poly(box)]
    green_boxes = [box for box in green_boxes if box_in_poly(box)]


    # Display boxes
    for box in blue_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 4)

    for box in red_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,0,255), 4)

    for box in green_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,255,0), 4)


    # GRASP TRANSFORM
    s = 619.88/475.71

    rgb_grasps = []
    for i, grasp in enumerate(grasps):
        rgb_grasps.append({'rgb_pixels': [240 + ((grasp[0] - 240) * s), 385 + ((grasp[1] - 320) * s)], 'grasp': grasp[2:]})
        cv2.circle(img, (int(rgb_grasps[i]['rgb_pixels'][1]), int(rgb_grasps[i]['rgb_pixels'][0])), 2, (0, 255,255), -1)

    rgb_grasps = sorted(rgb_grasps, key=lambda x: x['grasp'][-1], reverse=True)

    red_grasps = []
    for box in red_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                red_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (0, 0, 255), -1)
                break

    green_grasps = []
    for box in green_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                green_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (0, 255, 0), -1)
                break

    blue_grasps = []
    for box in blue_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                blue_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (255, 0, 0), -1)
                break

    cv2.imshow("Vision", img)
    cv2.waitKey(20)

    return red_grasps, green_grasps, blue_grasps

def calib():
    polys = []
    for pos in range(3):
        closed_loop_pos(homes[pos])
        polys.append(calibrate_triangle(pos))
    return polys

def calibrate_triangle(pos):
    msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
    min_black = np.array([0, 0, 0])
    max_black = np.array([50, 50, 50])

    cvb = cv_bridge.CvBridge()
    # Convert into opencv matrix
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')

    black_boxes = find_color(img, min_black, max_black, 50)
    poly_points = []
    for box in black_boxes:
        poly_points.append((box[0]+(box[2]/2), box[1]+(box[3]/2)))

    # print(poly_points)

    for p in poly_points:
        cv2.circle(img, tuple(p), 2, (255,255,0))
        # cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 3)
    # for box in black_boxes:

    cv2.imshow("Calib", img)
    cv2.waitKey(0)

    return poly_points



def closed_loop_pos(pos):
    msg = rospy.wait_for_message('/m1n6s200_driver/out/joint_angles', JointAngles)
    joints = [msg.joint1, msg.joint2, msg.joint3, msg.joint4, msg.joint5, msg.joint6, msg.joint7]

    while np.linalg.norm(np.asarray(joints) - np.asarray(pos)) > 2:
        move_to_joint_position(pos)
        msg = rospy.wait_for_message('/m1n6s200_driver/out/joint_angles', JointAngles)
        joints = [msg.joint1, msg.joint2, msg.joint3, msg.joint4, msg.joint5, msg.joint6, msg.joint7]
    print("Position Reached")

def robot_wrench_callback(msg):
    # Monitor wrench to cancel movement on collision.
    global MOVING
    if MOVING and msg.wrench.force.z < -2.0:
        MOVING = False
        rospy.logerr('Force Detected. Stopping.')
        kill_actions()


def pos_diff(a,b): return np.linalg.norm(np.array([a.x,a.y,a.z])-np.array([b.x,b.y,b.z]))

pos_for_check = None
pos_last_move = None

# Fixes Dougs Bug
def robot_position_callback(msg):
    # Monitor robot position.
    rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
    global CURR_Z, pos_for_check, pos_last_move, CURR_POS, CURR_ANG
    CURR_Z = msg.pose.position.z
    CURR_POS = msg.pose.position
    CURR_ANG = [msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w]

    if pos_for_check is None: pos_for_check = msg.pose.position
    if pos_last_move is None: pos_last_move = time.time()

    if pos_diff(pos_for_check, msg.pose.position) > 1e-2: pos_last_move = time.time()
    pos_for_check = msg.pose.position

    # if time.time() - pos_last_move > 10.0:
    #     p = msg.pose.position
    #     o = msg.pose.orientation
    #     print("RECOVERING FROM STALL")
    #     rospy.sleep(0.5)
    #     move_to_position([p.x+np.random.normal(0,0.01),
    #                       p.y+np.random.normal(0,0.01),
    #                       p.z+np.random.normal(0,0.01)], [o.x,o.y,o.z,o.w], timeout=0.5)
    #     rospy.sleep(0.5)
    #     print("RECOVER.....ED?")
    #     pos_for_check = msg.pose.position
    #     pos_last_move = time.time()


def move_to_pose(pose):
    # Wrapper for move to position.
    p = pose.position
    o = pose.orientation
    move_to_position([p.x, p.y, p.z], [o.x, o.y, o.z, o.w])


def execute_grasp(grasp):
    velo_pub = rospy.Publisher('/m1n6s200_driver/in/cartesian_velocity', kinova_msgs.msg.PoseVelocity, queue_size=1)
    # Execute a grasp.
    global MOVING
    global CURR_Z
    global CURR_POS

    global start_force_srv
    global stop_force_srv

    d = grasp
    # Calculate the gripper width.
    grip_width = d[4]
    # Convert width in pixels to mm.
    # 0.07 is distance from end effector (CURR_Z) to camera.
    # 0.1 is approx degrees per pixel for the realsense.
    g_width = 4 * ((CURR_Z + 0.07)) * np.tan(0.1 * grip_width / 2.0 / 180.0 * np.pi) * 1000
    # Convert into motor positions.
    g = min((1 - (min(g_width, 70)/70)) * (6800-4000) + 4000, 5500)
    set_finger_positions([g, g])

    rospy.sleep(0.5)

    # Pose of the grasp (position only) in the camera frame.
    gp = geometry_msgs.msg.Pose()
    gp.position.x = d[0]
    gp.position.y = d[1]
    gp.position.z = d[2]
    gp.orientation.w = 1

    # Convert to base frame, add the angle in (ensures planar grasp, camera isn't guaranteed to be perpendicular).
    gp_base = convert_pose(gp, 'camera_depth_optical_frame', 'm1n6s200_link_base')

    q = tft.quaternion_from_euler(np.pi, 0, d[3])
    gp_base.orientation.x = q[0]
    gp_base.orientation.y = q[1]
    gp_base.orientation.z = q[2]
    gp_base.orientation.w = q[3]

    publish_pose_as_transform(gp_base, 'm1n6s200_link_base', 'G', 0.5)

    # Offset for initial pose.
    initial_offset = 0.2
    gp_base.position.z += initial_offset

    # Disable force control, makes the robot more accurate.
    stop_force_srv.call(kinova_msgs.srv.StopRequest())

    # TRIED TO DO VELOCITY CONTROL - RIP
    #
    # raw_input()
    #
    # MOVING = True
    # threshold = 0.005
    # dx = gp_base.position.x - CURR_POS.x
    # dy = gp_base.position.y - CURR_POS.y
    # dz = gp_base.position.z - CURR_POS.z
    #
    #
    # dq = tft.quaternion_multiply(q, tft.quaternion_conjugate(CURR_ANG))
    # d_euler = tft.euler_from_quaternion(dq)
    #
    # while abs(dx) > threshold or abs(dy) > threshold or abs(dz) > threshold:
    #     dx = gp_base.position.x - CURR_POS.x
    #     dy = gp_base.position.y - CURR_POS.y
    #     dz = gp_base.position.z - CURR_POS.z
    #
    #     dq = tft.quaternion_multiply(q, tft.quaternion_conjugate(CURR_ANG))
    #     d_euler = tft.euler_from_quaternion(dq)
    #     print(d_euler)
    #
    #     d_euler = [np.arctan(np.tan(val)) for val in d_euler]
    #
    #     v = np.array([dx, dy, dz, d_euler[0], d_euler[1], d_euler[2]])
    #     v = np.clip(v, -0.1, 0.1)
    #     velo_pub.publish(kinova_msgs.msg.PoseVelocity(*v))
    #     rospy.sleep(1/100.0)
    #

    move_to_pose(gp_base)
    rospy.sleep(0.1)

    # Start force control, helps prevent bad collisions.
    start_force_srv.call(kinova_msgs.srv.StartRequest())

    rospy.sleep(0.25)

    # Reset the position
    gp_base.position.z -= initial_offset - 0.015
    MOVING = True
    move_to_pose(gp_base)

    # # Flag to check for collisions.
    # MOVING = True
    #
    # # Move straight down under velocity control.
    # while MOVING and CURR_Z - 0.02 > gp_base.position.z:
    #     dz = gp_base.position.z - CURR_Z - 0.05   # Offset by a few cm for the fingertips.
    #     MAX_VELO_Z = 0.08
    #     dz = max(min(dz, MAX_VELO_Z), -1.0*MAX_VELO_Z)
    #
    #     v = np.array([0, 0, dz, 0, 0, 0])
    #     velo_pub.publish(kinova_msgs.msg.PoseVelocity(*v))
    #     rospy.sleep(1/100.0)
    #
    MOVING = False
    #
    # close the fingers.
    rospy.sleep(0.1)
    set_finger_positions([8000, 8000])
    rospy.sleep(0.5)

    # Move back up to initial position.
    gp_base.position.z += initial_offset
    gp_base.orientation.x = 1
    gp_base.orientation.y = 0
    gp_base.orientation.z = 0
    gp_base.orientation.w = 0
    move_to_pose(gp_base)

    stop_force_srv.call(kinova_msgs.srv.StopRequest())

    return

centre = [
284.117645264,
182.481613159,
91.8198547363,
200.25,
-85.5681838989,
220.227279663,
0.0]

left = [
241.102935791,
182.481613159,
91.8198547363,
200.25,
-85.5681838989,
220.227279663,
0.0]

right = [
322.996337891,
182.481613159,
91.8198547363,
202.43182373,
-85.4318237305,
220.227279663,
0.0]

right_drop = [
322.941192627,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]
centre_drop = [
282.077209473,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]

left_drop = [
244.301467896,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]



# All homes and drops
homes = (left, centre, right)
drops = (left_drop,centre_drop, right_drop)

#==============================================================================
class SortEnv:
    def __init__(self, NL, OPL, TL):
        self.time_limit        = TL
        self.num_locations     = NL
        self.obj_per_loc       = OPL
        self.num_objects       = self.num_locations * self.obj_per_loc
        self.observation_space = spaces.Box(-np.inf, np.inf, (self.num_locations + self.num_objects*self.num_locations,))
        self.action_space      = spaces.Discrete(self.num_objects + self.num_locations)
        self.complete_reward   = 1.0
        self.slow = False

        # TODO: What is this?
        self.objects = {"Left": [], "Centre": [], "Right": []}

        # Image co-ords for the bin polygon masks
        # p = [[(16, 371), (565, 220), (92, 68), (335, 6)], [(608, 423), (35, 402), (463, 142), (190, 132)], [(627, 463), (97, 240), (589, 139), (341, 52)]]
        p = [[(325, 9), (4, 378), (552, 234), (76, 75)], [(612, 431), (27, 397), (470, 146), (192, 130)], [(635, 456),  (101, 218), (614, 124), (362, 34)]]
            # (467, 147), (189, 131)], [(84, 467), (634, 456), (21, 447), (101, 218), (614, 124), (361, 34)]]

        self.left_polygon   = p[0]
        self.centre_polygon = p[1]
        self.right_polygon  = p[2]

        # Map bin index to mask
        self.polygons = {0: self.left_polygon, 1: self.centre_polygon, 2: self.right_polygon}

        # Keep track of object class, location and grasp quality in each bin
        self.contents          = {0: [], 1: [], 2: []}
        self.failed_grasps     = 0
        self.successful_grasps = 0

    #--------------------------------------------------------------------------

    def reset(self):
        # Reset time
        self.timestep = 0

        # Open Hand
        self.grasped  = False
        set_finger_positions([0.0, 0.0])
        self.what_we_got = None

        rospy.sleep(0.25)

        # Return to centre position
        self.pos = 1
        # closed_loop_pos(centre)
        print("Homed")


        self._observe()
        return self.step(1)

    #--------------------------------------------------------------------------

    def _observe(self):
        # print("Observe")
        contents = []
        if self.grasped == 0:
            r, g, b= vision(self.polygons[self.pos])
            contents = []
            for obj in r: contents.append([1,0,0])
            for obj in g: contents.append([0,1,0])
            for obj in b: contents.append([0,0,1])
            self.contents[self.pos] = contents

            red_grasps = []
            green_grasps = []
            blue_grasps = []
            for obj in r: red_grasps.append(obj['grasp'])
            for obj in g: green_grasps.append(obj['grasp'])
            for obj in b: blue_grasps.append(obj['grasp'])
            self.grasps = [red_grasps, green_grasps, blue_grasps]


        agent_state = self.to_one_hot(self.pos) + [int(self.grasped)] + self.to_one_hot(self.what_we_got)
        return agent_state, contents

    #--------------------------------------------------------------------------

    def to_one_hot(self, index):
        l = [0]*self.num_locations
        if index != None:
            l[index] = 1
        return l

    #--------------------------------------------------------------------------

    def step(self, act):
        self.timestep += 1
        reward = 0

        action_dict = {0: "Left", 1: "Centre", 2: "Right", 3: "Drop", 4: "Red", 5: "Green", 6: "Blue"}

        if action_dict[act] == "Left": 
            print("Moving Left")
            self.pos = 0
            closed_loop_pos(left)

        if action_dict[act] == "Centre": 
            print("Moving Centre")
            self.pos = 1
            closed_loop_pos(centre)

        if action_dict[act] == "Right": 
            print("Moving Right")
            self.pos = 2
            closed_loop_pos(right)

        if self.grasped and action_dict[act] == "Drop":
            if self.what_we_got == self.pos:
                reward += 1
            else:
                reward -= 1

            self.what_we_got = None
            print("Dropping")
            self.grasped = 0
            closed_loop_pos(drops[self.pos])
            set_finger_positions([0.0, 0.0])
            rospy.sleep(0.25)
            closed_loop_pos(homes[self.pos])
        
        if self.grasped == False:
            attempted_grasp = False
            if action_dict[act] == "Red":
                print("Picking Red")
                if [1,0,0] in self.contents[self.pos]:
                    self.grasped = 1
                    self.what_we_got = 0
                    execute_grasp(self.grasps[0][0])
                    closed_loop_pos(homes[self.pos])
                    attempted_grasp = True

            if action_dict[act] == "Green":
                print("Picking Green")
                if [0,1,0] in self.contents[self.pos]:
                    self.grasped = 1
                    self.what_we_got = 1
                    execute_grasp(self.grasps[1][0])
                    closed_loop_pos(homes[self.pos])
                    attempted_grasp = True

            if action_dict[act] == "Blue":
                if [0,0,1] in self.contents[self.pos]:
                    print("Picking Blue")
                    self.grasped = 1
                    self.what_we_got = 2
                    execute_grasp(self.grasps[2][0])
                    closed_loop_pos(homes[self.pos])
                    attempted_grasp = True

            if attempted_grasp:
                self.grasped = detect_grasp()
                if not self.grasped:
                    self.failed_grasps += 1
                    set_finger_positions([0.0, 0.0])
                else:
                    self.successful_grasps += 1
                    if self.pos == self.what_we_got:
                        reward -= 1
                    else:
                        reward += 1
        
        return self._observe(), reward, self._done()

    def _done(self):
        bins_solved = 0
        for key in self.contents:
            correct = 0
            for o in self.contents[key]:
                if o[:3] == o[3:6]:
                    correct += 1
            bins_solved += int(correct == len(self.contents[key])) - 1*(int(len(self.contents[key]) == 0))
        return bins_solved == 3 or self.timestep >= self.time_limit

#=================================================================================
# Main loop
try:
    if __name__ == '__main__':
        print("Press Enter to Run")
        raw_input()
        rospy.init_node('ggcnn_open_loop_grasp')

        # Robot Monitors.
        wrench_sub = rospy.Subscriber('/m1n6s200_driver/out/tool_wrench', geometry_msgs.msg.WrenchStamped, robot_wrench_callback, queue_size=1)
        position_sub = rospy.Subscriber('/m1n6s200_driver/out/tool_pose', geometry_msgs.msg.PoseStamped, robot_position_callback, queue_size=1)

        # https://github.com/dougsm/rosbag_recording_services
        # start_record_srv = rospy.ServiceProxy('/data_recording/start_recording', std_srvs.srv.Trigger)
        # stop_record_srv = rospy.ServiceProxy('/data_recording/stop_recording', std_srvs.srv.Trigger)

        # Enable/disable force control.
        start_force_srv = rospy.ServiceProxy('/m1n6s200_driver/in/start_force_control', kinova_msgs.srv.Start)
        stop_force_srv = rospy.ServiceProxy('/m1n6s200_driver/in/stop_force_control', kinova_msgs.srv.Stop)

        rospy.sleep(0.25)
        # print(calib())
        get_image()

        env = SortEnv(3, 3, 500)

        # calibrate_depth()
        optimal = 20 # assuming rgb config in each bin

        # obs,_,_ = env.reset()
        # stacked_obs = agent.stack_obs(obs)

        print("STARTING")

        world_step = 0
        train_step = 0
        episode = 0
        while True:
            done           = False
            # Reset frame stacking
            closed_loop_pos(centre)
            obs, _, _      = env.reset()
            stacked_obs    = agent.stack_obs(obs)
            episode_reward = 0
            episode_length = 0
        
            while not done:
                # print(stacked_obs)
                # raw_input()

                # print("Contents", env.contents)
                # print("Obs", obs)
                # print("World step", world_step)
                print("Train step", train_step)
                # print("Buf", len(agent.buf))
                loc = env.pos
                # action = np.random.randint(0,7)
                action = agent.choose_action(stacked_obs, loc=loc, perfect=True)
                # print("Stacked Obs", agent.stacked_obs)
                # get_object_detections()

                nobs, rew, done = env.step(action)
                print("Reward:")
                print(rew)
                stacked_nobs    = agent.stack_obs(nobs)
            
                episode_reward += rew
                episode_length += 1

                nloc        = env.pos
                agent.add_transition(copy.deepcopy((stacked_obs, loc, action, rew, stacked_nobs, nloc, done)))
                stacked_obs = stacked_nobs

                #for i in range(len(agent.buf)//agent.BATCH):
                for i in range(1):
                    loss = agent.train()
                    if loss is not None: agent.writer.add_scalar("SortingTask/loss", loss, train_step)
                    train_step += 1

                agent.writer.add_scalar("SortingTask/eps", np.clip(1-agent.hours_elapsed()/agent.MH, 0.05, 1), world_step)
                agent.writer.add_scalar("SortingTask/reward", rew, world_step)

                # agent.writer.add_scalar("bamf/grasp_success", env.successful_grasps/(env.successful_grasps + env.failed_grasps + 1), world_step)

                world_step += 1

            # Reset stacked obs
            agent.stacked_obs = None
            print("############################### DONE ###############################")
            agent.writer.add_scalar("SortingTask/ep_rew", episode_reward, episode)
            agent.writer.add_scalar("SortingTask/ep_len", episode_length, episode)
            agent.writer.add_scalar("SortingTask/%_optimal", optimal/episode_length, episode)
            episode += 1
            print("-------------- Resetting the bins --------------")
            for r in range(50):
                action = np.random.randint(0,7)
                obs, rew, done = env.step(action)
            print("------- Done resetting the bins --------")



except:
    import traceback
    traceback.print_exc()
    # torch.save(agent.agent.state_dict(), "weight_final.pt")

