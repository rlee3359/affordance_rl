#! /usr/bin/env python3

from __future__ import print_function, division

import cv2, sys, os, numpy as np, torch, torch.nn as nn, torch.nn.functional as F, tensorboardX, random, copy, time
import rospy
sys.dont_write_bytecode = True

#==============================================================================
# PARAMS
#==============================================================================

BATCH  = 64  # batch size
TL     = 500 # time limit measured in discrete actions
E      = 128 # object instance embedding size
H      = 128 # full state embedding size
NL     = 3   # number of locations
OPL    = 3   # number of objects per designated location
MH     = 3   # number of hours to run for
FS     = 4   # frame stacking
VISION = False
start  = time.time()

#==============================================================================
# HELPERS
#==============================================================================

def deep_sets(state, instances):
    state = torch.from_numpy(np.asarray(state)).float()

    t_instances = []
    for i, instance in enumerate(instances):
        t_instances.append(torch.from_numpy(np.asarray(instance)).float())

    if len(instances) > 0:
        t_instances = torch.stack(t_instances).view(len(instances), env.instance_size)
        nodes = instance_encoder(t_instances)
        node = torch.sum(nodes, dim=0)
    else:
        node = torch.zeros(node_size)
    x = torch.cat([state, node], dim=0)
    return x

#------------------------------------------------------------------------------

def q_function(stacked_obs, loc):
    if VISION:
        imgs = [obs[1][0] for obs in stacked_obs]
        grps = [obs[1][1] for obs in stacked_obs]

        imgs = torch.cat([torch.from_numpy(np.transpose(img, [2,0,1]).astype(np.float32)/255)
                          for img in imgs], dim=0).unsqueeze(dim=0).cuda()
        grps = torch.cat([torch.from_numpy(np.transpose(grp, [2,0,1]).astype(np.float32)/255)
                          for grp in grps], dim=0).unsqueeze(dim=0).cuda()

        f =   viz_stack(imgs).view(-1)
        g = grasp_stack(grps).view(-1)
        encoded = torch.cat([f,g],dim=0)

        x = repr_encoder(torch.cat((encoded, torch.from_numpy(np.array(stacked_obs[-1][0])).float().cuda()), dim=0)).cpu().data
    else:
        reprs = []
        for obs in stacked_obs:
            state, objects, actions = obs
            # x  = deep_sets(state, objects)
            if len(objects) > 0:
                x  = torch.sum(torch.stack([instance_encoder(torch.from_numpy(np.array(inst)).float())
                                            for inst in objects]), dim=0)
            else:
                x  = torch.zeros(E)
            reprs.append(torch.cat([x, torch.from_numpy(np.array(state)).float()], dim=0))
        x  = torch.cat(reprs, dim=0)
        x  = repr_net(x)
    actions = stacked_obs[loc][2]
    qs = [q_net(torch.cat([x, torch.from_numpy(np.array(action)).float()], dim=0)) for action in actions]
    return qs

#------------------------------------------------------------------------------

def hours_elapsed():
    return ((time.time() - start)/60) / 60

#------------------------------------------------------------------------------

stacked_obs = None
def stack_obs_temporal(obs):
    global stacked_obs
    if stacked_obs is None: stacked_obs = [obs for _ in range(FS)]
    else                  : stacked_obs = stacked_obs[1:] + [obs]
    return copy.deepcopy(stacked_obs)

def stack_obs_spatial(obs):
    global stacked_obs
    if stacked_obs is None: stacked_obs = [[[0,0,0,0,0,0,0],[],[]] for _ in range(NL)]
    stacked_obs[np.argmax(obs[0][:3])] = obs
    return copy.deepcopy(stacked_obs)

stack_obs = stack_obs_temporal

#------------------------------------------------------------------------------

def choose_action(stacked_obs, loc, perfect=False):
    this_timestep = stacked_obs[-1]
    # this_timestep = stacked_obs[loc]
    state, obs, actions = this_timestep
    print(actions)
    eps = np.clip(1-hours_elapsed()/MH, 0.05, 1)
    if perfect: eps = 0.05
    print("Epsilon: ", eps)
    if np.random.random() < eps:
        print("Selecting random action...")
        qidx = np.random.randint(len(actions))
    else:
        qs = [q.data.item() for q in q_function(stacked_obs, -1)]
        qidx = np.argmax(qs)
    return qidx, actions[qidx]

#------------------------------------------------------------------------------

def add_transition(transition):
    buf.append(transition)

#------------------------------------------------------------------------------

def train():
    if len(buf) < BATCH: return None
    print("Training")
    batch = random.sample(buf, BATCH)
    losses = []
    for stacked_obs, loc, act, rew, stacked_nobs, nloc, done in batch:
        qs  = q_function(stacked_obs, -1)
        # print("BATCH ELEMENT")
        # print("act", act)
        # print("qs", qs)
        # print("stacked_obs", stacked_obs)
        # print("loc", loc)
        qt  = qs[act]

        qn  = torch.stack(q_function(stacked_nobs, -1))
        qm  = torch.max(qn)

        y   = rew + (1-done)*0.9*qm.detach()
        tde = y - qt

        loss = tde**2
        losses.append(loss)

    loss = torch.mean(torch.stack(losses))
    opt.zero_grad()
    loss.backward()
    opt.step()

    torch.save(agent.state_dict(), 'weights.pt')

    return loss.data.item()

#==============================================================================
# SETUP
#==============================================================================

instance_size = 7
act_size      = 8
state_size    = 7
cin           = 3
obs_size      = (84, 84, 3)

if VISION:
    # visual encoder
    viz_stack       = nn.Sequential(nn.Conv2d(cin*FS, 32, 8, stride=4), nn.ReLU(),
                                    nn.Conv2d(    32, 64, 4, stride=2), nn.ReLU(),
                                    nn.Conv2d(    64, 64, 3, stride=1), nn.ReLU()).cuda()

    grasp_stack     = nn.Sequential(nn.Conv2d(cin*FS, 32, 8, stride=4), nn.ReLU(),
                                    nn.Conv2d(    32, 64, 4, stride=2), nn.ReLU(),
                                    nn.Conv2d(    64, 64, 3, stride=1), nn.ReLU()).cuda()

    shape           = (1, obs_size[2]*FS, obs_size[0], obs_size[1])
    conv_out        = viz_stack(torch.zeros(shape).cuda())
    conv_size       = conv_out.view(-1).shape[0]

    repr_encoder     = nn.Sequential(nn.Linear(conv_size*2+state_size, 512), nn.ReLU(),
                                     nn.Linear(512, H)).cuda()
else:
    # set and state encoder
    instance_encoder = nn.Sequential(nn.Linear(instance_size,128),    nn.ReLU(),
                                     nn.Linear(128, E),               nn.Tanh())
    repr_net         = nn.Sequential(nn.Linear((E+state_size)*FS, H), nn.ReLU())

# for policy gradient
query_net        = nn.Linear(H, act_size)
temperature_net  = nn.Linear(H, 1)

# for q-learning
q_net            = nn.Sequential(nn.Linear(H+act_size, 128), nn.ReLU(),
                                 nn.Linear(128, 128),        nn.ReLU(),
                                 nn.Linear(128, 1))

if VISION: agent = nn.ModuleList([viz_stack, grasp_stack, repr_encoder, query_net, temperature_net, q_net])
else:      agent = nn.ModuleList([instance_encoder, repr_net, query_net, temperature_net, q_net])
opt   = torch.optim.Adam(agent.parameters(), lr=1e-4)
# except:
#     import traceback
#     traceback.print_exc()


# target nets
#instance_encoder_target = copy.deepcopy(instance_encoder)
#repr_net_target = copy.deepcopy(repr_net)
#q_net_target = copy.deepcopy(q_net)

buf = []
writer = tensorboardX.SummaryWriter()
#
# #==============================================================================
# # RUN
# #==============================================================================
#
# if __name__ == "__main__":
#     print("MAINING")
#     episode = 0; step = 0; num_actions = 0
#     while hours_elapsed() < MH:
#         obs = env.reset(); done = False; reward = 0; length = 0
#         while not done:
#             act_idx, act = choose_action(obs)
#             nobs, rew, done, info = env.step(act)
#
#             buf.append(copy.deepcopy((obs, act_idx, rew, nobs, done)))
#             obs = nobs
#
#             for loss_iteration in range(1):
#                 loss = train(buf)
#                 if loss is not None: writer.add_scalar("affordances/loss", loss, step)
#                 step += 1
#
#             reward += rew; length += 1; num_actions += 1
#
#         writer.add_scalar("affordances/reward", reward, episode)
#         writer.add_scalar("affordances/length", length, episode)
#         writer.add_scalar("affordances/hours",  hours_elapsed(), episode)
#
#         episode += 1
#
#
