#! /usr/bin/env python3

from __future__ import print_function, division

import cv2, sys, os, numpy as np, torch, torch.nn as nn, torch.nn.functional as F, tensorboardX, random, copy, time
sys.dont_write_bytecode = True

#==============================================================================
# PARAMS
#==============================================================================

BATCH  = 64  # batch size
TL     = 500 # time limit measured in discrete actions
E      = 128 # object instance embedding size
H      = 128 # full state embedding size
NL     = 3   # number of locations
OPL    = 3   # number of objects per designated location
MH     = 0.5   # number of hours to run for
FS     = 4   # frame stacking
VISION = False
start  = time.time()

#==============================================================================
# HELPERS
#==============================================================================


def q_function(stacked_obs, loc):
    reprs = []
    for obs in stacked_obs:
        state, objects = obs
        state = np.array(state)
        objects = np.array(objects).flatten()
        z = np.zeros((10*3,))
        z[:len(objects)] = objects
        reprs.append(torch.from_numpy(np.concatenate((state, z), axis=0)).float())
    x = torch.cat(reprs, dim=0)
    qs = q_net(x)

    return qs

#------------------------------------------------------------------------------

def hours_elapsed():
    return ((time.time() - start)/60) / 60

#------------------------------------------------------------------------------

stacked_obs = None
def stack_obs_temporal(obs):
    global stacked_obs
    if stacked_obs is None: stacked_obs = [obs for _ in range(FS)]
    else                  : stacked_obs = stacked_obs[1:] + [obs]
    return copy.deepcopy(stacked_obs)

def stack_obs_spatial(obs):
    global stacked_obs
    if stacked_obs is None: stacked_obs = [[[0,0,0,0,0,0,0],[]] for _ in range(NL)]
    stacked_obs[np.argmax(obs[0][:3])] = obs
    return copy.deepcopy(stacked_obs)

stack_obs = stack_obs_temporal

#------------------------------------------------------------------------------

def choose_action(stacked_obs, loc, perfect=False):
    this_timestep = stacked_obs[-1]
    # this_timestep = stacked_obs[loc]
    state, obs = this_timestep
    eps = np.clip(1-hours_elapsed()/MH, 0.05, 1)
    if perfect: eps = 0.05
    print("Epsilon: ", eps)
    if np.random.random() < eps:
        print("Selecting random action...")
        qidx = np.random.randint(act_size)
    else:
        qs = q_function(stacked_obs, -1)
        qidx = torch.argmax(qs).data.numpy()
        print(qidx)
    return int(qidx)

#------------------------------------------------------------------------------

def add_transition(transition):
    buf.append(transition)

#------------------------------------------------------------------------------

def train():
    if len(buf) < BATCH: return None
    print("Training")
    batch = random.sample(buf, BATCH)
    losses = []
    for stacked_obs, loc, act, rew, stacked_nobs, nloc, done in batch:
        qs  = q_function(stacked_obs, -1)
        qt  = qs[act]

        qn  = q_function(stacked_nobs, -1)
        qm  = torch.max(qn)

        y   = rew + (1-done)*0.9*qm.detach()
        loss = nn.functional.smooth_l1_loss(qt, y)
        losses.append(loss)

    loss = torch.mean(torch.stack(losses))
    opt.zero_grad()
    loss.backward()
    opt.step()

    # torch.save(agent.state_dict(), 'nds_weights.pt')

    return loss.data.item()

#==============================================================================
# SETUP
#==============================================================================

instance_size = 3
act_size      = 7
state_size    = 7
input_size    = 4*(instance_size * 10 + state_size)
cin           = 3
obs_size      = (84, 84, 3)

# for q-learning
q_net            = nn.Sequential(nn.Linear(input_size, 128), nn.ReLU(),
                                 nn.Linear(128, 128),        nn.ReLU(),
                                 nn.Linear(128, act_size))

agent = nn.ModuleList([q_net])
agent.load_state_dict(torch.load('nds_weights.pt'))
opt   = torch.optim.Adam(agent.parameters(), lr=1e-4)

# except:
#     import traceback
#     traceback.print_exc()


# target nets
#instance_encoder_target = copy.deepcopy(instance_encoder)
#repr_net_target = copy.deepcopy(repr_net)
#q_net_target = copy.deepcopy(q_net)

buf = []
writer = tensorboardX.SummaryWriter()
