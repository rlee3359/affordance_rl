#! /usr/bin/zsh

source ~/playpen-ws/devel/setup.zsh

rosbag record -o /media/guest/vault/affordance_rl/SortingTask \
    /usb_cam/image_raw/compressed \
    /camera/rgb/image_raw/compressed \
    /ggcnn/img/grasps \
    /ggcnn/out/command \
-e '/m1n6s200_driver/out/.*'


