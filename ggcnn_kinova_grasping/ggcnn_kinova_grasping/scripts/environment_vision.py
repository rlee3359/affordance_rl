#! /usr/bin/env python

from __future__ import print_function
import rospy
import tf.transformations as tft
from gym import spaces

import numpy as np
import cv_bridge
import cv2
from kinova_msgs.msg import PoseVelocity
from kinova_msgs.msg import JointAngles

import kinova_msgs.msg
import kinova_msgs.srv
import sensor_msgs.msg
from sensor_msgs.msg import Image
import std_msgs.msg
import std_srvs.srv
import geometry_msgs.msg

from helpers.gripper_action_client import set_finger_positions
from helpers.position_action_client import position_client, move_to_position, move_to_joint_position
from helpers.transforms import current_robot_pose, publish_tf_quaterion_as_transform, convert_pose, publish_pose_as_transform
from helpers.covariance import generate_cartesian_covariance

import agent
import copy
import time
import torch

np.random.seed(0)

cvb = cv_bridge.CvBridge()

def calibrate_depth():
    cv2.namedWindow('image')
    def callback(x): pass

    # create trackbars for color change
    cv2.createTrackbar('Depth','image',0,1000,callback)

    while(1):
        msg = rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
        img = cvb.imgmsg_to_cv2(msg)

        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        # get current positions of four trackbars
        # d = cv2.getTrackbarPos('Depth','image')
        # d = float(d)/1000
        d = 0.265
        grasp_cut = 0.03

        print("Depth", d)

        _, mask = cv2.threshold(img, d,1, cv2.THRESH_BINARY_INV)
        # print("MeanMask", np.mean(mask))
        # print(np.mean(mask) > 0.03)

        cv2.imshow('image', mask)


def detect_grasp():
    print("Detecting Grasp")
    cvb = cv_bridge.CvBridge()
    msg = rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
    img = cvb.imgmsg_to_cv2(msg)

    d = 0.265
    grasp_cut = 0.03

    _, mask = cv2.threshold(img, d,1, cv2.THRESH_BINARY_INV)
    return np.mean(mask) > 0.03

def calibrate_colors():
    cvb = cv_bridge.CvBridge()
    cv2.namedWindow('image')
    def callback(x): pass

    # create trackbars for color change
    cv2.createTrackbar('R High','image',0,255,callback)
    cv2.createTrackbar('R Low','image',0,255,callback)
    cv2.createTrackbar('G High','image',0,255,callback)
    cv2.createTrackbar('G Low','image',0,255,callback)
    cv2.createTrackbar('B High','image',0,255,callback)
    cv2.createTrackbar('B Low','image',0,255,callback)

    while(1):
        msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
        img = cvb.imgmsg_to_cv2(msg, 'bgr8')


        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        # get current positions of four trackbars
        r_low = cv2.getTrackbarPos('R Low','image')
        r_hi = cv2.getTrackbarPos('R High','image')
        g_low = cv2.getTrackbarPos('G Low','image')
        g_hi = cv2.getTrackbarPos('G High','image')
        b_low = cv2.getTrackbarPos('B Low','image')
        b_hi = cv2.getTrackbarPos('B High','image')
        min_colors = np.array([b_low, g_low, r_low])
        max_colors = np.array([b_hi, g_hi, r_hi])

        print("Min colors", min_colors)
        print("Max colors", max_colors)

        binary  = cv2.inRange(img, min_colors, max_colors)
        eroded  = cv2.erode(binary, kernel, iterations=num_iter)
        dilated = cv2.dilate(eroded, kernel, iterations=num_iter)

        viz_img = img.copy()
        viz_img[dilated <= 0] = 0
        cv2.imshow('image', viz_img)


MOVING = False  # Flag whether the robot is moving under velocity control.
CURR_Z = 0  # Current end-effector z height.

kernel = np.ones((2,2), np.uint8)
num_iter = 2
im_size = [480, 640]

def find_color(img, min_colors, max_colors, min_area):
    global vel, im_size, point_reached
    global centroid, state

    # Threshold the image for blue
    binary  = cv2.inRange(img, min_colors, max_colors)
    eroded  = cv2.erode(binary, kernel, iterations=num_iter)
    dilated = cv2.dilate(eroded, kernel, iterations=num_iter)
    # cv2.imshow("Thresh", dilated)

    # Find the contours - saved in blue_contours and red_contours
    im2, contours, hierachy= cv2.findContours(dilated.copy(), cv2.RETR_TREE,
                                                         cv2.CHAIN_APPROX_SIMPLE)

    boxes = []
    if len(contours) > 0 and contours is not None:
        boxes = detect_centroids(contours, min_area)

    return boxes

def detect_centroids(contours, minimum_area):
    boxes = []
    # minimum_area=8000
    cx=[]
    cy=[]
    # Find the bounding box for each contour
    for contour in contours:
        x,y,w,h = cv2.boundingRect(contour)
        area = w*h
        if area > minimum_area:
            boxes.append([x,y,w,h])

    return boxes

def vision(polygon):
    print("Vision")
    import time
    rospy.sleep(0.1)
    grasps = _GRASPS
    grasps = grasps.data.split('|')

    # TODO:
    # Jake:
    # not sure if this is the right thing to do, but it'll keep last night's crash from happening
    try:
        grasps = [[float(g) for g in grasp.split(',')] for grasp in grasps]
    except ValueError:
        return [],[],[]
    # TODO:



    min_blue = np.array([42,0,0])
    max_blue = np.array([255,95,31])

    min_red = np.array([0,0,31])
    max_red = np.array([33,76,255])

    min_green = np.array([0,0,2])
    max_green = np.array([50,255,53])

    img = _RGB_IMG.copy()


    # Find colors
    red_boxes = find_color(img, min_red, max_red, 5000)
    green_boxes = find_color(img, min_green, max_green, 4000)
    blue_boxes = find_color(img, min_blue, max_blue, 6000)

    # cv2.imshow("Vision", img)
    # cv2.waitKey(0)


    im_size = img.shape
    mask = np.ones(im_size)
    polygon = sorted(polygon, key = lambda x: x[0])
    cv2.polylines(img, [np.int32(np.array(polygon))], True, (0,255,255))
    cv2.fillConvexPoly(mask, np.asarray(polygon), (0,0,0))
    img = cv2.addWeighted(img/255.0, 0.8, 1-mask, 0.3, -0.3)
    # img[mask[:]!=(255,255,255)] = 0

    # Display boxes
    for box in blue_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 1)

    for box in red_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,0,255), 1)

    for box in green_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,255,0), 1)


    def box_in_poly(box):
        pts = [[box[0], box[1]], [box[0], box[1]+box[3]], [box[0]+box[2], box[1]+box[3]], [box[0]+box[2], box[1]]]
        p_mask = np.zeros(im_size)
        rect_mask = np.zeros(im_size)
        cv2.fillConvexPoly(p_mask, np.asarray(polygon), (255,255,255))
        cv2.fillConvexPoly(rect_mask, np.asarray(pts), (255,255,255))
        intersect = cv2.bitwise_and(p_mask, rect_mask)
        return np.sum(intersect) > np.sum(rect_mask)/3

        # pts = [(box[0], box[1]), (box[0], box[1]+box[3]), (box[0]+box[2], box[1]), (box[0]+box[2], box[1]+box[3])]
        # num_in = [cv2.pointPolygonTest(np.array([polygon]), pt, False) for pt in pts]
        # num_in = sum([i for i in num_in if i >= 0])
        # return num_in > 1

    blue_boxes  = [box for box in blue_boxes  if box_in_poly(box)]
    red_boxes   = [box for box in red_boxes   if box_in_poly(box)]
    green_boxes = [box for box in green_boxes if box_in_poly(box)]


    # Display boxes
    for box in blue_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 4)

    for box in red_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,0,255), 4)

    for box in green_boxes:
        cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,255,0), 4)


    # GRASP TRANSFORM
    s = 619.88/475.71

    rgb_grasps = []
    for i, grasp in enumerate(grasps):
        rgb_grasps.append({'rgb_pixels': [240 + ((grasp[0] - 240) * s), 385 + ((grasp[1] - 320) * s)], 'grasp': grasp[2:]})
        cv2.circle(img, (int(rgb_grasps[i]['rgb_pixels'][1]), int(rgb_grasps[i]['rgb_pixels'][0])), 2, (0, 255,255), -1)

    rgb_grasps = sorted(rgb_grasps, key=lambda x: x['grasp'][-1], reverse=True)

    red_grasps = []
    for box in red_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                red_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (0, 0, 255), -1)
                break

    green_grasps = []
    for box in green_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                green_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (0, 255, 0), -1)
                break

    blue_grasps = []
    for box in blue_boxes:
        for grasp in rgb_grasps:
            if (box[0] < grasp['rgb_pixels'][1] < box[0]+box[2]) and (box[1] < grasp['rgb_pixels'][0] < box[1]+box[3]):
                blue_grasps.append(grasp)
                cv2.circle(img, (int(grasp['rgb_pixels'][1]), int(grasp['rgb_pixels'][0])), 3, (255, 0, 0), -1)
                break

    cv2.imshow("Vision", img)
    cv2.waitKey(20)

    return red_grasps, green_grasps, blue_grasps

def calib():
    polys = []
    for pos in range(3):
        closed_loop_pos(homes[pos])
        polys.append(calibrate_triangle(pos))
    return polys

def calibrate_triangle(pos):
    msg = rospy.wait_for_message('/camera/rgb/image_raw', sensor_msgs.msg.Image)
    min_black = np.array([0, 0, 0])
    max_black = np.array([50, 50, 50])

    cvb = cv_bridge.CvBridge()
    # Convert into opencv matrix
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')

    black_boxes = find_color(img, min_black, max_black, 50)
    poly_points = []
    for box in black_boxes:
        poly_points.append((box[0]+(box[2]/2), box[1]+(box[3]/2)))

    # print(poly_points)

    for p in poly_points:
        cv2.circle(img, tuple(p), 2, (255,255,0))
        # cv2.rectangle(img, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (255,0,0), 3)
    # for box in black_boxes:

    cv2.imshow("Calib", img)
    cv2.waitKey(0)

    return poly_points



def closed_loop_pos(pos):
    msg = rospy.wait_for_message('/m1n6s200_driver/out/joint_angles', JointAngles)
    joints = [msg.joint1, msg.joint2, msg.joint3, msg.joint4, msg.joint5, msg.joint6, msg.joint7]

    while np.linalg.norm(np.asarray(joints) - np.asarray(pos)) > 2:
        move_to_joint_position(pos)
        msg = rospy.wait_for_message('/m1n6s200_driver/out/joint_angles', JointAngles)
        joints = [msg.joint1, msg.joint2, msg.joint3, msg.joint4, msg.joint5, msg.joint6, msg.joint7]
    print("Position Reached")

def robot_wrench_callback(msg):
    # Monitor wrench to cancel movement on collision.
    global MOVING
    if MOVING and msg.wrench.force.z < -2.0:
        MOVING = False
        rospy.logerr('Force Detected. Stopping.')

def pos_diff(a,b): return np.linalg.norm(np.array([a.x,a.y,a.z])-np.array([b.x,b.y,b.z]))

pos_for_check = None
pos_last_move = None

# Fixes Dougs Bug
def robot_position_callback(msg):
    # Monitor robot position.
    rospy.wait_for_message('/camera/depth/image_meters', sensor_msgs.msg.Image)
    global CURR_Z, pos_for_check, pos_last_move
    CURR_Z = msg.pose.position.z

    if pos_for_check is None: pos_for_check = msg.pose.position
    if pos_last_move is None: pos_last_move = time.time()

    if pos_diff(pos_for_check, msg.pose.position) > 1e-2: pos_last_move = time.time()
    pos_for_check = msg.pose.position

    if time.time() - pos_last_move > 10.0:
        p = msg.pose.position
        o = msg.pose.orientation
        print("RECOVERING FROM STALL")
        rospy.sleep(0.5)
        move_to_position([p.x+np.random.normal(0,0.01),
                          p.y+np.random.normal(0,0.01),
                          p.z+np.random.normal(0,0.01)], [o.x,o.y,o.z,o.w], timeout=0.5)
        rospy.sleep(0.5)
        print("RECOVER.....ED?")
        pos_for_check = msg.pose.position
        pos_last_move = time.time()


def move_to_pose(pose):
    # Wrapper for move to position.
    p = pose.position
    o = pose.orientation
    move_to_position([p.x, p.y, p.z], [o.x, o.y, o.z, o.w])


def execute_grasp(grasp):
    # Execute a grasp.
    global MOVING
    global CURR_Z
    global start_force_srv
    global stop_force_srv

    d = grasp

    # Calculate the gripper width.
    grip_width = d[4]
    # Convert width in pixels to mm.
    # 0.07 is distance from end effector (CURR_Z) to camera.
    # 0.1 is approx degrees per pixel for the realsense.
    g_width = 4 * ((CURR_Z + 0.07)) * np.tan(0.1 * grip_width / 2.0 / 180.0 * np.pi) * 1000
    # Convert into motor positions.
    g = min((1 - (min(g_width, 70)/70)) * (6800-4000) + 4000, 5500)
    set_finger_positions([g, g])

    rospy.sleep(0.5)

    # Pose of the grasp (position only) in the camera frame.
    gp = geometry_msgs.msg.Pose()
    gp.position.x = d[0]
    gp.position.y = d[1]
    gp.position.z = d[2]
    gp.orientation.w = 1

    # Convert to base frame, add the angle in (ensures planar grasp, camera isn't guaranteed to be perpendicular).
    gp_base = convert_pose(gp, 'camera_depth_optical_frame', 'm1n6s200_link_base')

    q = tft.quaternion_from_euler(np.pi, 0, d[3])
    gp_base.orientation.x = q[0]
    gp_base.orientation.y = q[1]
    gp_base.orientation.z = q[2]
    gp_base.orientation.w = q[3]

    publish_pose_as_transform(gp_base, 'm1n6s200_link_base', 'G', 0.5)

    # Offset for initial pose.
    initial_offset = 0.2
    gp_base.position.z += initial_offset

    # Disable force control, makes the robot more accurate.
    stop_force_srv.call(kinova_msgs.srv.StopRequest())

    move_to_pose(gp_base)
    rospy.sleep(0.1)

    # Start force control, helps prevent bad collisions.
    start_force_srv.call(kinova_msgs.srv.StartRequest())

    rospy.sleep(0.25)

    # Reset the position
    gp_base.position.z -= initial_offset

    # Flag to check for collisions.
    MOVING = True

    # Move straight down under velocity control.
    velo_pub = rospy.Publisher('/m1n6s200_driver/in/cartesian_velocity', kinova_msgs.msg.PoseVelocity, queue_size=1)
    while MOVING and CURR_Z - 0.02 > gp_base.position.z:
        dz = gp_base.position.z - CURR_Z - 0.03   # Offset by a few cm for the fingertips.
        MAX_VELO_Z = 0.08
        dz = max(min(dz, MAX_VELO_Z), -1.0*MAX_VELO_Z)

        v = np.array([0, 0, dz, 0, 0, 0])
        velo_pub.publish(kinova_msgs.msg.PoseVelocity(*v))
        rospy.sleep(1/100.0)

    MOVING = False

    # close the fingers.
    rospy.sleep(0.1)
    set_finger_positions([8000, 8000])
    rospy.sleep(0.5)

    # Move back up to initial position.
    gp_base.position.z += initial_offset
    gp_base.orientation.x = 1
    gp_base.orientation.y = 0
    gp_base.orientation.z = 0
    gp_base.orientation.w = 0
    move_to_pose(gp_base)

    #stop_force_srv.call(kinova_msgs.srv.StopRequest())

    return


action_dict = {'move_left'  : [1,0,0, 0, 0,0,0],
               'move_centre': [0,1,0, 0, 0,0,0],
               'move_right' : [0,0,1, 0, 0,0,0],

               'pick_red'   : [0,0,0, 0, 1,0,0],
               'pick_green' : [0,0,0, 0, 0,1,0],
               'pick_blue'  : [0,0,0, 0, 0,0,1],

               'drop'       : [0,0,0, 1, 0,0,0]}


centre = [
284.117645264,
182.481613159,
91.8198547363,
200.25,
-85.5681838989,
220.227279663,
0.0]

left = [
241.102935791,
182.481613159,
91.8198547363,
200.25,
-85.5681838989,
220.227279663,
0.0]

right = [
322.996337891,
182.481613159,
91.8198547363,
202.43182373,
-85.4318237305,
220.227279663,
0.0]

right_drop = [
322.941192627,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]
centre_drop = [
282.077209473,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]

left_drop = [
244.301467896,
220.863967896,
100.036766052,
194.18182373,
-64.1590957642,
204.0,
0.0]





# HACK HACK DIE DIE I HATE THIS
def obj_is_grasp(obj, grasp):
    return abs(obj[-1]-grasp[-1]) < 0.01





# All homes and drops
homes = (left, centre, right)
drops = (left_drop,centre_drop, right_drop)

#==============================================================================
class SortEnv:
    def __init__(self, NL, OPL, TL):

        self.time_limit        = TL
        self.num_locations     = NL
        self.obj_per_loc       = OPL
        self.num_objects       = self.num_locations * self.obj_per_loc
        self.observation_space = spaces.Box(-np.inf, np.inf, (self.num_locations + self.num_objects*self.num_locations,))
        self.action_space      = spaces.Discrete(self.num_objects + self.num_locations)
        # self.remove_penalty    = -0.05
        # self.placement_reward  =  0.025
        # self.bin_reward        =  0.15
        # self.bin_penalty       = -0.3
        self.complete_reward   =  1.0
        # self.class_colors = [(0,0,255), (255,0,0), (0,255,0), (0,64,128)]
        self.slow = False

        # TODO: What is this?
        self.objects = {"Left": [], "Centre": [], "Right": []}

        # Predefined cartesian locations for home and drop positions
        # self.centre      = ([ 0.067, -0.174, 0.358], [0.985, -0.016, -0.030, 0.170])
        # self.centre_drop = ([ 0.003, -0.346, 0.127], [0.997,  0.017, -0.017, 0.076])
        # self.left        = ([ 0.179, -0.092, 0.356], [0.927,  0.336,  0.029, 0.165])
        # self.left_drop   = ([ 0.223, -0.250, 0.111], [0.929,  0.367,  0.006, 0.052])
        # self.right       = ([-0.071, -0.189, 0.356], [0.916, -0.365, -0.088, 0.142])
        # self.right_drop  = ([-0.246, -0.244, 0.127], [0.925, -0.372, -0.046, 0.064])
        #


        # Predefined Joint Locations for home and drop locations

        # self.left_polygon    = [(111, 457), (608, 340), (233, 10)]
        # self.centre_polygon  = [(579, 453), (51, 435), (236, 14), (307, 8), (374, 6)]
        # self.right_polygon   = [(534, 472), (39, 334), (384, 5)]
        # p = [[(16, 377), (624, 300), (88, 50), (364, 31)], [(612, 416), (23, 392), (448, 120), (201, 101)], [(608, 452), (13, 275), (573, 99), (348, 17)]]
        # p = [[(0, 361), (545, 216), (74, 58), (316, 0)], [(599, 418), (25, 394), (453, 136), (182, 127)], [(612, 457), (85, 231), (580, 132), (332, 43)]]
        p = [[(16, 371), (565, 220), (92, 68), (335, 6)], [(608, 423), (35, 402), (463, 142), (190, 132)], [(627, 463), (97, 240), (589, 139), (341, 52)]]
        self.left_polygon = p[0]
        self.centre_polygon = p[1]
        self.right_polygon = p[2]

        # Map bin index to mask
        self.polygons = {0: self.left_polygon, 1: self.centre_polygon, 2: self.right_polygon}

        # Keep track of object class, location and grasp quality in each bin
        self.contents = {0: [], 1: [], 2: []}
        self.failed_grasps = 0
        self.successful_grasps = 0

    #--------------------------------------------------------------------------

    def reset(self):
        self.timestep = 0
        self.grasped  = False
        #self.objects  = np.random.randint(0, self.num_locations, (self.num_objects,))

        set_finger_positions([0.0, 0.0])
        self.what_we_got = None
        rospy.sleep(0.25)

        # Return to Centre
        self.pos = 1
        # closed_loop_pos(centre)
        print("Homed")


        #self._observe()
        # action = [0,0,0,0] + self.contents[self.pos][0][3:]
        # self.step(action)

        # # Evaluate number of correct/incorrect
        # num_correct, num_incorrect = self._reward()
        # self.score = num_correct - num_incorrect
        # print(num_correct, num_incorrect, self.score)
        #
        # while self._done(): return self.reset()
        return self.step([0,1,0,0,0,0,0,0])

    #--------------------------------------------------------------------------

    def _observe(self):
        # print("Observe")
        contents = []
        if self.grasped == 0:
            r, g, b= vision(self.polygons[self.pos])
            contents = []
            for obj in r: contents.append(self.to_one_hot(self.pos) + [1,0,0, obj['grasp'][-1]])
            for obj in g: contents.append(self.to_one_hot(self.pos) + [0,1,0, obj['grasp'][-1]])
            for obj in b: contents.append(self.to_one_hot(self.pos) + [0,0,1, obj['grasp'][-1]])

            grasps = []
            for obj in r: grasps.append(obj['grasp'])
            for obj in g: grasps.append(obj['grasp'])
            for obj in b: grasps.append(obj['grasp'])
            self.grasps = grasps

            self.contents[self.pos] = contents

        img = (cv2.resize(_RGB_IMG, (84,84)), cv2.resize(_GRASP_IMG, (84,84))) # we cache these in the subscriber

        agent_state = self.to_one_hot(self.pos) + [self.grasped] + [0]*3 # empty state variables for image-based agent

        return agent_state, img #, contents

    #--------------------------------------------------------------------------

    def to_one_hot(self, index):
        l = [0]*self.num_locations
        if index != None:
            l[index] = 1
        return l

    #--------------------------------------------------------------------------

    def step(self, act):
        self.timestep += 1
        reward = 0
        print("Stepping")
        objs = []

        action_possibilities = []
        if   act[:-1] == action_dict['move_left']:
            print("Moving Left")
            self.pos = 0
            closed_loop_pos(left)
            state, imgs = self._observe()

            action_possibilities.append(action_dict['move_centre'] + [0])
            action_possibilities.append(action_dict['move_right'] + [0])
            if not self.grasped:
                for content in self.contents[self.pos]:
                    action_possibilities.append([0,0,0,0] + content[3:])
                # print("Self.contents:", self.contents[self.pos])

        elif act[:-1] == action_dict['move_centre']:
            print("Moving Centre")
            self.pos = 1
            closed_loop_pos(centre)
            state, imgs = self._observe()

            action_possibilities.append(action_dict['move_left'] + [0])
            action_possibilities.append(action_dict['move_right'] + [0])
            if not self.grasped:
                for content in self.contents[self.pos]:
                    action_possibilities.append([0,0,0,0] + content[3:])
                # print("Self.contents:", self.contents[self.pos])

        elif act[:-1] == action_dict['move_right']:
            print("Moving Right")
            self.pos = 2
            closed_loop_pos(right)
            state, imgs = self._observe()

            action_possibilities.append(action_dict['move_left'] + [0])
            action_possibilities.append(action_dict['move_centre'] + [0])
            if self.grasped == 0:
                for content in self.contents[self.pos]:
                    action_possibilities.append([0,0,0,0] + content[3:])
                # print("Self.contents:", self.contents[self.pos])

        elif act[:-1] == action_dict['drop']:
            if self.to_one_hot(self.what_we_got) == self.to_one_hot(self.pos):
                reward += 1
            else:
                reward -= 1

            self.what_we_got = None
            print("Dropping")
            self.grasped = 0
            closed_loop_pos(drops[self.pos])
            set_finger_positions([0.0, 0.0])
            rospy.sleep(0.25)
            closed_loop_pos(homes[self.pos])

            state, imgs = self._observe()


            if self.pos == 0:
                action_possibilities.append(action_dict['move_right'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            elif self.pos == 1:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_right'] + [0])
            elif self.pos == 2:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])

            for content in self.contents[self.pos]:
                action_possibilities.append([0,0,0,0] + content[3:])

        elif act[:-1] == action_dict['pick_red']:
            # If you picked up the object from its assigned bin, negative reward, else positive
            self.grasped = 1
            self.what_we_got = 0
            print("Pick Red")
            # For object in the current positions contents
            for obj in self.contents[self.pos]:
                # If the color and grasp quality of the action is the same as the current iterated object
                if obj[3:] == act[4:]:
                    # Iterate over possible grasps, find the one with the same grasp quality (again, wtf???)
                    for grasp in self.grasps:
                        if obj_is_grasp(obj, grasp):
                            execute_grasp(grasp)
                            break
            closed_loop_pos(homes[self.pos])
            if self.pos == 0:
                action_possibilities.append(action_dict['move_right'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            elif self.pos == 1:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_right'] + [0])
            elif self.pos == 2:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            self.grasped = detect_grasp()
            if not self.grasped:
                self.failed_grasps += 1
                set_finger_positions([0.0, 0.0])
            else:
                self.successful_grasps += 1
                if self.pos == 0:
                    reward -= 1
                else:
                    reward += 1
            state, imgs = self._observe()

        elif act[:-1] == action_dict['pick_green']:
            self.grasped = 1
            self.what_we_got = 1
            print("Pick Green")
            for obj in self.contents[self.pos]:
                if obj[3:] == act[4:]:
                    for grasp in self.grasps:
                        if obj_is_grasp(obj, grasp):
                            execute_grasp(grasp)
                            break
            closed_loop_pos(homes[self.pos])
            if self.pos == 0:
                action_possibilities.append(action_dict['move_right'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            elif self.pos == 1:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_right'] + [0])
            elif self.pos == 2:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            self.grasped = detect_grasp()
            if not self.grasped:
                self.failed_grasps += 1
                set_finger_positions([0.0, 0.0])
            else:
                self.successful_grasps += 1
                if self.pos == 1:
                    reward -= 1
                else:
                    reward += 1

            state, imgs = self._observe()

        elif act[:-1] == action_dict['pick_blue']:
            self.grasped = 1
            self.what_we_got = 2
            print("Pick Blue")
            for obj in self.contents[self.pos]:
                if obj[3:] == act[4:]:
                    for grasp in self.grasps:
                        if obj_is_grasp(obj, grasp):
                            execute_grasp(grasp)
                            break
            closed_loop_pos(homes[self.pos])
            if self.pos == 0:
                action_possibilities.append(action_dict['move_right'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            elif self.pos == 1:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_right'] + [0])
            elif self.pos == 2:
                action_possibilities.append(action_dict['move_left'] + [0])
                action_possibilities.append(action_dict['move_centre'] + [0])
            self.grasped = detect_grasp()
            if not self.grasped:
                self.failed_grasps += 1
                set_finger_positions([0.0, 0.0])
            else:
                self.successful_grasps += 1
                if self.pos == 2:
                    reward -= 1
                else:
                    reward += 1

            state, imgs = self._observe()

        else:
            print("Action Undefined")

        if self.grasped:
            print("GRASPED!")
            action_possibilities.append(action_dict['drop'] + [0])
        else:
            print("Empty Grippers")
        # What are the possible actions? Give to agent

        print("Reward:", reward)
        # state, obs = self._observe()
        observation = state, imgs, action_possibilities

        done = self._done()
        if done and self.timestep < self.time_limit:
            reward += 1

        return observation, reward, done

    def _done(self):
        bins_solved = 0
        for key in self.contents:
            correct = 0
            for o in self.contents[key]:
                if o[:3] == o[3:6]:
                    correct += 1
            bins_solved += int(correct == len(self.contents[key])) - 1*(int(len(self.contents[key]) == 0))
        return bins_solved == 3 or self.timestep >= self.time_limit


    #--------------------------------------------------------------------------
    #
    # def num_correct(self):
    #     return sum([int(self._target_location(object_id) == location and self.grasped != object_id) for object_id,location in enumerate(self.objects)])
    #
    # def num_incorrect(self):
    #     return sum([int(self._target_location(object_id) != location and self.grasped != object_id) for object_id,location in enumerate(self.objects)])
    #
    # def _bins_solved(self):
    #     bins_solved = 0
    #     for b in range(self.num_locations):
    #         bin_solved = True
    #         for o in range(b*self.obj_per_loc,(b+1)*self.obj_per_loc):
    #             if self._target_location(o) != self.objects[o]: bin_solved = False
    #         if bin_solved: bins_solved += 1
    #     return bins_solved

    #--------------------------------------------------------------------------

    # def _reward(self):
    #     closed_loop_pos(left)
    #     red, green, blue = vision(self.left_polygon)
    #     num_blue_left  = len(blue)
    #     num_red_left   = len(red)
    #     num_green_left = len(green)
    #
    #     print("Left Bin")
    #     print("Blue", num_blue_left)
    #     print("Red", num_red_left)
    #     print("Green", num_green_left)
    #
    #
    #     closed_loop_pos(centre)
    #     red, green, blue = vision(self.centre_polygon)
    #     num_blue_centre  = len(blue)
    #     num_red_centre   = len(red)
    #     num_green_centre = len(green)
    #
    #     print("Right Bin")
    #     print("Blue", num_blue_centre)
    #     print("Red", num_red_centre)
    #     print("Green", num_green_centre)
    #
    #
    #     closed_loop_pos(right)
    #     red, green, blue = vision(self.right_polygon)
    #     num_blue_right  = len(blue)
    #     num_red_right   = len(red)
    #     num_green_right = len(green)
    #
    #     print("Right Bin")
    #     print("Blue", num_blue_right)
    #     print("Red", num_red_right)
    #     print("Green", num_green_right)
    #
    #     num_correct = num_red_left + num_green_centre + num_blue_right
    #     num_incorrect = (num_blue_left + num_green_left) + (num_blue_centre + num_red_centre) + (num_red_right + num_green_right)
    #
    #     return num_correct, num_incorrect
    #
    #

    #--------------------------------------------------------------------------

    #
    # def _target_location(self, object_id):
    #     return object_id // self.obj_per_loc
    #
    # #--------------------------------------------------------------------------
    #
    # def _obs(self):
    #     obs = np.zeros(self.observation_space.shape, np.float32)
    #     if self.grasped is not None: obs[self._target_location(self.grasped)] = 1
    #     for i in range(self.num_objects): obs[self.num_locations+i*self.num_locations+self.objects[i]] = 1
    #     return obs
    #
    # #--------------------------------------------------------------------------
    #
    # def _all_correct(self):
    #     return all([(self._target_location(object_id) == location) for object_id,location in enumerate(self.objects)])
    #
    # #--------------------------------------------------------------------------
    #
    # def _done(self):
    #     return (self.timestep >= self.time_limit or self._all_correct())
    #
    # #--------------------------------------------------------------------------
    #
    # def _info(self):
    #     pass














_RGB_IMG = np.zeros((84,84,3), np.uint8)
def handle_rgb(msg):
    global _RGB_IMG
    cvb = cv_bridge.CvBridge()
    img = cvb.imgmsg_to_cv2(msg, 'bgr8')
    _RGB_IMG = img





_GRASP_IMG = np.zeros((84,84,3), np.uint8)
def handle_grasp_img(msg):
    global _GRASP_IMG
    grasps = cvb.imgmsg_to_cv2(msg)
    _GRASP_IMG = grasps


_GRASPS = ""
def handle_grasps(msg):
    global _GRASPS
    _GRASPS = msg





#=================================================================================
# Main loop
try:
    if __name__ == '__main__':
        rospy.init_node('ggcnn_open_loop_grasp')

        # Robot Monitors.
        wrench_sub   = rospy.Subscriber('/m1n6s200_driver/out/tool_wrench', geometry_msgs.msg.WrenchStamped,   robot_wrench_callback, queue_size=1)
        position_sub = rospy.Subscriber('/m1n6s200_driver/out/tool_pose',   geometry_msgs.msg.PoseStamped,   robot_position_callback, queue_size=1)

        # image topics
        rgb_sub       = rospy.Subscriber("/camera/rgb/image_raw", sensor_msgs.msg.Image, handle_rgb,       queue_size=1)
        grasp_img_sub = rospy.Subscriber("/ggcnn/img/grasp_img",  sensor_msgs.msg.Image, handle_grasp_img, queue_size=1)

        # dougrasp topic
        grasp_sub = rospy.Subscriber('/ggcnn/img/grasps', std_msgs.msg.String, handle_grasps, queue_size=1)

        # https://github.com/dougsm/rosbag_recording_services
        # start_record_srv = rospy.ServiceProxy('/data_recording/start_recording', std_srvs.srv.Trigger)
        # stop_record_srv = rospy.ServiceProxy('/data_recording/stop_recording', std_srvs.srv.Trigger)

        # Enable/disable force control.
        start_force_srv = rospy.ServiceProxy('/m1n6s200_driver/in/start_force_control', kinova_msgs.srv.Start)
        stop_force_srv = rospy.ServiceProxy('/m1n6s200_driver/in/stop_force_control', kinova_msgs.srv.Stop)

        rospy.sleep(0.25)
        # print(calib())

        env = SortEnv(3, 3, 500)

        optimal = 20 # assuming rgb config in each bin

        # obs,_,_ = env.reset()
        # stacked_obs = agent.stack_obs(obs)

        print("STARTING")

        world_step = 0
        train_step = 0
        episode = 0
        while True:
            done           = False
            # Reset frame stacking
            closed_loop_pos(centre)
            obs, _, _      = env.reset()
            stacked_obs    = agent.stack_obs(obs)
            episode_reward = 0
            episode_length = 0
            while not done:
                # print("Contents", env.contents)
                # print("Obs", obs)
                # print("World step", world_step)
                print("Train step", train_step)
                # print("Buf", len(agent.buf))
                loc = env.pos
                act_idx, action = agent.choose_action(stacked_obs, loc=loc, perfect=False)
                # print("Stacked Obs", agent.stacked_obs)
                # perf_idx, perf_action = agent.choose_action(stacked_obs, perfect=True)

                # _,perf_rew,_ = copy.deepcopy(env).step(perf_action)

                nobs, rew, done = env.step(action)
                stacked_nobs    = agent.stack_obs(nobs)

                episode_reward += rew
                episode_length += 1

                nloc        = env.pos
                agent.add_transition(copy.deepcopy((stacked_obs, loc, act_idx, rew, stacked_nobs, nloc, done)))
                stacked_obs = stacked_nobs

                #for i in range(len(agent.buf)//agent.BATCH):
                for i in range(1):
                    loss = agent.train()
                    if loss is not None: agent.writer.add_scalar("SortingTask/loss", loss, train_step)
                    train_step += 1

                agent.writer.add_scalar("SortingTask/eps", np.clip(1-agent.hours_elapsed()/agent.MH, 0.05, 1), world_step)
                agent.writer.add_scalar("SortingTask/reward", rew, world_step)

                # agent.writer.add_scalar("bamf/perf_reward", perf_rew, world_step)
                # agent.writer.add_scalar("bamf/grasp_success", env.successful_grasps/(env.successful_grasps + env.failed_grasps + 1), world_step)

                world_step += 1

            # Reset stacked obs
            agent.stacked_obs = None
            print("############################### DONE ###############################")
            agent.writer.add_scalar("SortingTask/ep_rew", episode_reward, episode)
            agent.writer.add_scalar("SortingTask/ep_len", episode_length, episode)
            agent.writer.add_scalar("SortingTask/%_optimal", optimal/episode_length, episode)
            episode += 1
            print("-------------- Resetting the bins --------------")
            for r in range(50):
                action = obs[2][np.random.randint(len(obs[2]))]
                obs, rew, done = env.step(action)
            print("------- Done resetting the bins --------")



except:
    import traceback
    traceback.print_exc()
    torch.save(agent.agent.state_dict(), "weight_final.pt")

